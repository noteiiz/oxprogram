import java.util.Scanner;

public class OX {
    static char table [][] ={
            {'-','-','-'},
            {'-','-','-'},
            {'-','-','-'},
    };
    static int row, col;
    static char player = 'x';
    static int turn = 0;
    public static void main(String[] args) {
        showWelcome();
        while (true){
            showTable();    // <<== Table 3x3
            showTurn();
            input();        // ==>> row col , player
           if(checkWin()) { //  True
             break;
            }
           switchPlayer();
        }
        showTable();
        showWin();
        showBye();

    }

    private static void switchPlayer() {
        player = player=='x'?'o':'x';
        turn++;
    }

    private static void showBye() {
        System.out.println("Well play, Good bye.");
    }

    private static void showWin() {
        if(isDraw()){
            System.out.println("DRAW" );
        }else {
            System.out.println(player + " win . . . .");
        }
    }

    private static boolean checkWin() {
        if(checkRow()){
            return true;
        }
        if(checkCol()){
            return true;
        }
        if(checkX()){
            return true;
        }
        if (isDraw()){
            return true;
        }
        return false;

    }

    private static boolean isDraw() {
        if(turn==8) return true;
        return false;
    }


    private static boolean checkX() {
        if(checkX1()){
            return true;
        }
        if(checkX2()){
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for(int i=0; i< table.length;i++){
            if(table[i][i]!=player) return false;
        }
        return true;
    }

    private static boolean checkX2() {
        for(int i=0; i< table.length; i++){
            if(table[i][2-i]!=player) return false;
        }
        return true;
    }

    private static boolean checkRow(int rowInd) {
        for(int colInd =0; colInd<table[rowInd].length; colInd++){
            if(table[rowInd][colInd] !=player) return false;
        }
        return true;
    }
    private static boolean checkRow() {
        for(int rowInd =0; rowInd< table.length; rowInd++){
            if(checkRow(rowInd)) return true;
        }
        return false;
    }
    private static boolean checkCol(int colInd) {
        for(int rowInd =0; rowInd<table[colInd].length; rowInd++){
            if(table[rowInd][colInd] !=player) return false;
        }
        return true;
    }
    private static boolean checkCol() {
        for(int colInd =0; colInd< table[0].length; colInd++){
            if(checkCol(colInd)) return true;
        }
        return false;
    }

    private static void input() {
        while (true){
            try{
                Scanner sc = new Scanner(System.in);
                System.out.print("Please input Row Col: ");
                String input = sc.nextLine();
                String str[] = input.split(" ");
                if(str.length!=2){
                    System.out.println("Error: Please input Row Col (Ex. 1 1)");
                    continue;
                }
                row = Integer.parseInt(str[0]);
                col = Integer.parseInt(str[1]);
                if(row>=3 || row<=1 || col>3 || col<1){
                    System.out.println("Error: Please input Row Col in between 1-3");
                    continue;
                }
                if(!setMark()){
                    System.out.println("Error: Please input another Row Col (Ex. 1 1)");
                    continue;
                }
                break;

            }catch (Exception e){
                System.out.println("Error: Please input Row Col (Ex. 1 1)");
                continue;
            }

        }

    }
    private static boolean setMark(){
        if(table[row-1][col-1]!='-'){
            return false;
        }
        table[row-1][col-1] = player;
        return true;
    }
    private static void showTurn() {
        System.out.println(player + " turn");
    }

    private static void showTable() {
        System.out.println("  1 2 3");
        for(int rowInd = 0; rowInd<table.length; rowInd++){
            System.out.print(rowInd+1);
            for(int colInd = 0; colInd<table[rowInd].length; colInd++){
                System.out.print(" "+table[rowInd][colInd]); // - - -
            }
            System.out.println();
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to the OX Game");
    }
}
